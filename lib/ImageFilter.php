<?php

namespace SteeveDroz\CiUploads;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class ImageFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        // Do something here
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        $path = WRITEPATH . $request->uri->getPath();
        $mime = mime_content_type($path);
        if (explode('/', $mime)[0] == 'image') {
            switch ($mime) {
                case 'image/jpeg':
                    $image = imagecreatefromjpeg($path);
                    break;
            }

            $watermark = config('\SteeveDroz\CiUploads\Config\SDCi4Uploads')->watermark;
            $font = __DIR__ . '/Ubuntu-Regular.ttf';

            $semiWhite = imagecolorallocatealpha($image, 255, 255, 255, 96);
            $semiBlack = imagecolorallocatealpha($image, 0, 0, 0, 96);

            $textSize = imagettfbbox(100, 0, $font, $watermark);
            $textWidth = $textSize[2] - $textSize[0];
            $textHeight = $textSize[1] - $textSize[7];

            $rotatedTextSize = imagettfbbox(100, 90, $font, $watermark);
            $rotatedTextWidth = $rotatedTextSize[0] - $rotatedTextSize[6];
            $rotatedTextHeight = $rotatedTextSize[1] - $rotatedTextSize[3];

            $width = imagesx($image);
            $height = imagesy($image);

            $ratioNormal = min(($width - 20) / $textWidth, ($height - 20) / $textHeight);
            $ratioRotated = min(($width - 20) / $rotatedTextWidth, ($height - 20) / $rotatedTextHeight);

            if ($ratioNormal > $ratioRotated) {
                $ratio = $ratioNormal;
                $rotation = 0;
                $x = ($width - $textWidth * $ratio) / 2;
                $y = ($height + $textHeight * $ratio) / 2;
            } else {
                $ratio = $ratioRotated;
                $rotation = 90;
                $x = ($width + $rotatedTextWidth * $ratio) / 2;
                $y = ($height + $rotatedTextHeight * $ratio) / 2;
            }

            imagettftext($image, 100.0 * $ratio, $rotation, $x - 1, $y - 1, $semiWhite, $font, $watermark);
            imagettftext($image, 100.0 * $ratio, $rotation, $x + 1, $y + 1, $semiBlack, $font, $watermark);

            switch ($mime) {
                case 'image/jpeg':
                    imagejpeg($image);
                    break;
            }
        }
    }
}
