<?php namespace SteeveDroz\CiUploads;

use App\Controllers\BaseController;

class Upload extends BaseController
{
    public function fetch(...$file)
    {
        $path = WRITEPATH.'uploads/'.implode('/', $file);
        if (file_exists($path)) {
            $this->response->setHeader('Content-Type', mime_content_type($path));
            echo file_get_contents($path);
        }
    }
}
